# The input is a list of dictionaries where each dictionary represents an object in space that has a probability of smashing into earth. You're going help sort out the more dangerous objects for further analysis. Follow this pseudo-code:

# big_objects = new list
# for each of the input space objects
#     if the object's estimated diameter is less than 1, skip it
#     create a new dictionary with these fields (and values) from the space-object:
#         * `estimated_diameter`
#         * `impact_probability`
#     encoded_object = json encode the dictionary
#     add encoded_object to the big_objects list
# return the big_objects list

import json
def find_big_space_objects(space_objects):
    big_objects = []
    for space_object in space_objects:
        if space_object["estimated_diameter"] < 1.0:
            continue
        big_objects.append(json.dumps({
            "estimated_diameter": space_object["estimated_diameter"],
            "impact_probability": space_object["impact_probability"],
        }))
    print(big_objects)
    return big_objects






# This function, like the previous, takes a list of car dictionaries as an input. However, instead of filtering the list, we're going to sort it by the make of the car.

# If we have an input like this:

# cars = [
#     {"make": "ford", "model": 8, "year": 1955},
#     {"make": "chevy", "model": 12, "year": 1964},
#     {"make": "ford", "model": 11, "year": 1978},
#     {"make": "chevy", "model": 19, "year": 2000},             
# ]
# your function will produce this as the output:

# output = {
#     "ford": [
#         {"make": "ford", "model": 8, "year": 1955},
#         {"make": "ford", "model": 11, "year": 1978},        
#     ],
#     "chevy": [
#         {"make": "chevy", "model": 12, "year": 1964},
#         {"make": "chevy", "model": 19, "year": 2000},             
#     ]
# }
# Note that the output is a dictionary where each key is the make of a car and the value is a list of the car-dictionaries that have that make.

# Here is pseudocode for the function:

# sort_by_make(cars)
#     results = empty dictionary
#     for each car in cars
#         make = the value for the "make" key for this car
#         if results does NOT have make as a key
#             results[make] = empty list
#         add the car to the results[make] list
    
#     Quiz: is there anything else to do?

#     return the results list

def sort_by_make(cars):
    results = {}
    for car in cars:
        make = car["make"]
        if not make in results:
            results[make] = []
        results[make].append(car)
    return results