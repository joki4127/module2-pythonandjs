# This is kind-of like the second problem, but a little different...

# This function takes a list of dictionaries (items) and dictionary of filters (filters). It should return a filtered copy of items with only the items that match any of the filters.

# Example:

# items = [
#     {"color":"blue", "size":"small"},
#     {"color":"red", "size":"small"},
#     {"color":"purple", "size":"medium"},
#     {"color":"green", "size":"large"},
# ]
# filters = {
#     "color": "blue",
#     "size": "medium"
# }

# result = only_items_with(items, filters)
# print(result) # --> [
#     {"color":"blue", "size":"small"},
#     {"color":"purple", "size":"medium"},
# ]

def only_items_with(items, filters):

    # loop over the things, get the things you want
    result = []
    for item in items:
        keep = False
        for key, value in filters.items():
            if item.get(key) == value:
                keep = True
                break
        if keep:
            result.append(item)
    return result
    
    # for item in items:
    #     for key in filters.keys():
    #         if item[key] == filters[key]:
    #             result.append(item)
    #             break
    # return result