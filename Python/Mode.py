# The mode average is the number that occurs most often in a list of numbers. For example, the mode average

# Please complete the mode function to find the mode average of an array of numbers. The array will never be empty. There will only be one modal value.

# Example inputs and outputs:

# Input	Output
# [1, 1, 2, 3]	1
# [3, 2, 1, 3]	3
# [10]	10

def mode(numbers):
    if len(numbers) == 0:
        return float("nan")
    num_count = {}
    for num in numbers:
        if num not in num_count:
            num_count[num] = 0
        num_count[num] += 1

    max = 0
    max_num = None
    for key, value in num_count.items():
        if value > max:
            max = value
            max_num = key
    return max_num