# This pattern of selecting some of the items from an input list is so common that the ability to do so is built into Python's list comprehensions.

# Here's an example that selects all of the numbers from a list that are greater than 4.

# numbers = [1,2,3,4,5,6,7,8]

# greater_than_4 = [number for number in numbers if number > 4]

# print(greater_than_4)  # --> [5,6,7,8]
# The if number > 4 at the end is what does the filtering.

# This example will select all of the strings that represent a number:

# strings = ["cat", "1", "a", "5.3"]

# number_strings = [string for string in strings if string.isnumeric()]

# print(number_strings)  # --> ["1", "5.3"]
# Here if string.isnumeric() is the part that's specifying which elements should be included.

# Implement the function shorter_than(strings, max_length) in Python using a list comprehension.

def shorter_than_X(strings, max_length):
    return [string for string in strings if len(string) < max_length]




# Please complete the following function that takes a list of values and a threshold value and returns a new list that contains entries from the original list that are less than or equal to the threshold value.

# Values	Threshold	Output
# []	7	[]
# [1, 10, 20]	7	[1]
# [1, 10, 20]	10	[1, 10]
# [20, 30, 40]	10	[]
# [20, 30, 40]	10	[]
# ["a", "d", "z"]	"c"	["a"]

def filter_less_than_or_equal_to(values, threshold):
    return [value for value in values if value <= threshold]