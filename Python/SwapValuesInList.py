# Replace the comment "swap the items" with code that will swap the 
# first two items if the first item in the list is greater 
# than the second item in the list.

# For example:
# If the input is [1, 2], it should return [1, 2].
# If the input is [4, 2], then it should return [2, 4]

def sort(items):
    if items[0] > items[1]:
        (items[0], items[1]) = (items[1], items[0])
    return items

#Whole list swap

def sort(items):
    for i in range(len(items) - 1):
        if items[i] > items[i + 1]:
            (items[i], items[i + 1]) = (items[i + 1], items[i])
    return items

# We almost have a sort here.
# If you continually perform this swapping routine over the entire list, 
# it will eventually end up sorted.
# Add a variable that will get set to True if a swap happens inside of the for loop.
# After the loop, if no swaps were performed, we know the list is sorted 
# and we can break out of the loop and return the results.
# You use the break keyword to break out of a loop, in case you've forgotten.

def sort(items):
    while True:
        made_a_swap = False
        for i in range(len(items) - 1):
            if items[i] > items[i+1]:
                (items[i], items[i+1]) = (items[i+1], items[i])
                made_a_swap = True
        if not made_a_swap:
            break
    return items