# Please complete the following function that takes a list of values and a multiplier and returns a new list with each element of the original list multiplied by the multiplier.

# Values	Multiplier	Output
# []	7	[]
# [1, 10, 20]	7	[7, 70, 140]
# ["a", "d", "z"]	2	["aa", "dd", "zz"]

def multiply_map(values, multiplier):
    return [v * multiplier for v in values]

#OR

def multiply_map(values, multiplier):
    l = []
    for value in values:
        value = multiplier * value
        l.append(value)
    return l