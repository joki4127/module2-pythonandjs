# Now, let's do it in Python!

# Write a function named repeats that checks if a string repeats itself.

# repeats should take one argument, s, the string to test
# For this exercise, you can assume that s is a string
# If the first half of s equals the second half, then return True
# If s is an empty string, then return true
# Otherwise, return False

def repeats(s):
    length = len(s)
    half = length // 2
    for index, letter in enumerate(s):
        if index >= half:
            break
        if s[index] != s[index + half]:
            return False
    return True

    # def repeats(s):
#     length = len(s)
#     mid = int(length / 2)
#     if (len(s) == 0):
#         return True
#     if len(s) % 2 == 1:
#         return False
#     else:
#         if (s[0: mid] == s[mid:]):
#             return True
#         return False