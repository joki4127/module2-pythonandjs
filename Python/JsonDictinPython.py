# Please complete the following function that accepts a JSON-formatted string representing a dictionary and a key. The function should return True if the key exists in the dictionary represented by the JSON, and False otherwise.

# JSON-formatted string	Property name	Output
# ''	"name"	False
# '{"age": 23}'	"name"	False
# '{"name": "Noor"}'	'name"	True

import json

def has_key(json_string, key_name):
    x = json.loads(json_string)
    for key in x:
        if key == key_name:
            return True
    return False

#OR

import json

def has_key(json_string, key_name):
    content = json.loads(json_string)
    return key_name in content