# Create a function named third_largest_value that accepts a single parameter, values.

# The values parameter will be a list
# Find the third largest value in the list
# If the length of the list is less than 3, return None

def third_largest_value(values):
    if len(values) < 3:
        return None
    values = sorted(values)
    return values[-3]