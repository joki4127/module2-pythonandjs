#Implement a function called swap(), which will swap two items in a list.
#It takes a list and two indexes and will swap the values at those two indices and then return the list.

#Examples:
#lst = [8,11]
#print(swap(lst, 0, 1)) #  --> [11,8]

def swap(items, idx_1, idx_2):
    items[idx_1], items[idx_2] = items[idx_2], items[idx_1]
    return items


# Write a function, bubble_up() that will do one pass through the list swapping elements as needed to move the larger items toward the end of the list.

# Example:
# lst = [4,3,2,1]
# print(bubble_up(lst)) # --> [3,2,1,4]

def bubble_up(items):
    for i in range(len(items)-1):
        if items[i] > items[i+1]:
            items[i] , items[i+1] = items[i+1] , items[i]
    return items


# Write a function called bubble_down() that works the same as bubble_up() except that it starts at the end of the list and works its way towards the beginning of the list. This function will move smaller elements from the end of the list towards the beginning.

# Example:
# lst = [4,3,2,1]
# print(bubble_down(lst)) # --> [1,4,3,2]

def bubble_down(items):
    for i in range(len(items)-1)[::-1]:
        if items[i] > items[i+1]:
            items[i] , items[i+1] = items[i+1] , items[i]
    return items


# Finish off the implementation of the function cocktail_sort() here.
# Do not try to call your functions above. That won't work.
# DO use the code from your functions above (cut and paste :-) ).
# Do refer back to bubble_sort() at the top for reference.

def cocktail_sort(items):
    while True:
        made_a_swap = False

        # bubble up
        # ENTER YOUR CODE Here
        # TO DO A BUBBLE UP
        # MAKE SURE TO SET made_a_swap if needed
        for i in range(len(items)-1):
            if items[i] > items[i+1]:
                items[i] , items[i+1] = items[i+1] , items[i]
                made_a_swap = True

        if not made_a_swap:
            break

        # bubble down
        # ENTER YOUR CODE Here
        # TO DO A BUBBLE DOWN
        # MAKE SURE TO SET made_a_swap if needed
        for i in range(len(items)-1)[::-1]:
            if items[i] > items[i+1]:
                items[i] , items[i+1] = items[i+1] , items[i]
                made_a_swap = True
    
        if not made_a_swap:
            break

    return items