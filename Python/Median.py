# The median average is the value that occurs in the middle of a sorted list of numbers. For example, given this list:

# [1, 10, 2, 8, 4]

# its median average is 4 because, once its sorted, 4 is the middle value.

# If you have an even number of items in the list, you find the middle two, then take the mean average of those. For example, if you have this list:

# [1, 10, 2, 8, 4, 6]

# its median average is 5, which is the mean average of 4 and 6.

# Please complete the median function below to calculate the median average of an array of numbers. The list will never be empty.

# Example inputs and outputs:

# Input	Output
# [1, 2, 3]	2
# [3, 2, 1]	2
# [5, 3, 1, 1]	2


def median(numbers):
    if len(numbers) == 0:
        return float("nan")
    nums_copy = sorted(numbers)
    if len(nums_copy) % 2 == 0:
        half_len = (len(nums_copy) / 2)
        num1, num2 = nums_copy[int(half_len-1): int(half_len+1)]
        return (num1 + num2) / 2
    else:
        middle = int(len(nums_copy) / 2)
        return nums_copy[middle]
        

# import math
# def median(numbers):
#     if len(numbers) == 0:
#         return None
        
#     if len(numbers) % 2 == 1:
#         middle = math.floor(len(numbers) / 2)
#         return sorted_nums[middle]
        
#     if len(numbers) % 2 == 0: