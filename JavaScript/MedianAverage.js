// The median average is the value that occurs in the middle of a sorted list of numbers. For example, given this list:

// [1, 10, 2, 8, 4]

// its median average is 4 because, once its sorted, 4 is the middle value.

// If you have an even number of items in the list, you find the middle two, then take the mean average of those. For example, if you have this list:

// [1, 10, 2, 8, 4, 6]

// its median average is 5, which is the mean average of 4 and 6.

// Please complete the median function below to calculate the median average of an array of numbers. The list will never be empty.

function median(numbers) {
    const numsCopy = [...numbers];
    numsCopy.sort(function (val1, val2) { return val1 - val2 });
    if (numsCopy.length % 2 === 0) {
      const halfLen = numsCopy.length / 2;
      const [num1, num2] = numsCopy.slice(halfLen - 1, halfLen + 1);
      return (num1 + num2) / 2;
    } else {
      const middle = Math.floor(numsCopy.length / 2);
      return numsCopy[middle];
    }
  }
  
  // function median(numbers) {
  //     let length = numbers.length
  //     numbers.sort((a, b) => a - b)
  //     if (length % 2 === 1) {
  //         return numbers[Math.floor(length / 2)]
  //     } else {
  //         let first = numbers[length/2 - 1]
  //         let second = numbers[length/2]
  //         let average = (first + second) / 2
  //         return average
  //     }
  // }