// The mode average is the number that occurs most often in a list of numbers. For example, the mode average in the following list is 4:

// [1, 4, 10, 2, 8, 4, 6, 4]

// Please complete the mode function to find the mode average of an array of numbers. The array will never be empty. There will only be one modal value.

function mode(numbers) {
    const numCount = {};
    for (const num of numbers) {
       if (numCount[num] === undefined) {
          numCount[num] = 0;
       }
       numCount[num] += 1;
    }
 
    let max = null;
    let maxNum = null;
    for (const num in numCount) {
       if (numCount[num] > max) {
          max = numCount[num];
          maxNum = num;
       }
    }
    return maxNum;
 }
 
 
 // function mode(numbers {
 //     let mode = null;
 //     const counter = {};
     
 //     for (let num of numbers) {
 //         if counter(num === undefined) {
 //             counter[num] = 1;
 //         } else {
 //             counter[num] += 1;
 //         }
 //     }
 
 //     let numMode = Object.values(counter);
     
 //     let modeCount = Math.max(...numMode);
     
 //     for (const c in counter) {
 //         if (counter[c] === modeCount) {
 //             return c;
 //         }
 //     }
 // }