// Please complete the following function that takes a list of values and a multiplier and returns a new list with each element of the original list multiplied by the multiplier.

// Values	Multiplier	Output
// []	7	[]
// [1, 10, 20]	7	[7, 70, 140]

function multiplyMap(values, multiplier) {
    return values.map(x => x * multiplier);
}