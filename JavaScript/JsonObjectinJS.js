// Please complete the following function that accepts a JSON-formatted string representing an object and the name of a property. The function should return true if the property exists in the object represented by the JSON, and false otherwise.

// JSON-formatted string	Property name	Output
// ''	"name"	false
// '{"age": 23}'	"name"	false
// '{"name": "Noor"}'	'name"	true

function hasProperty(jsonString, propertyName) {
    const content = JSON.parse(jsonString);
    return propertyName in content;
  }