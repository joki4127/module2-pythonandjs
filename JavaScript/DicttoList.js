// Please complete the following function that takes a list of objects and a property name. The function should return a list of the value for the property in each object in the list. If the object does not have the property in it, then it should return undefined in that object's position.

// List of objects	Property	Output
// [{age: 10}, {age: 12}]	"age"	[10, 12]
// [{age: 10}, {remote: false}]	"age"	[10, undefined]
// []	"age"	[]

function valuesForProperty(values, propertyName) {
    return values.map(x => x[propertyName])
}
