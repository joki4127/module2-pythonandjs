// Write a function named repeats that checks if a string repeats itself.

// repeats should take one argument, s, the string to test
// For this exercise, you can assume that s is a string
// If the first half of s equals the second half, then return true
// If s is an empty string, then return true
// Otherwise, return false
// Here's a link to the methods of String  on MDN.

function repeats(s) {
    // your code here
    var length = s.length;
    var half = Math.floor(length / 2);
    for (var i = 0; i < half; i += 1) {
      if (s[i] != s[half + i]) {
        return false;
      }
    }
    return true;
  }
  
  // function repeats(s) {
//     if (s === ""){
//         return true;
//     }
//     let part1 = s.slice(0, Math.floor(s.length/2));
//     let part2 = s.slice(Math.floor(s.length/2), s.length);
//     return part2 === part1;
// }