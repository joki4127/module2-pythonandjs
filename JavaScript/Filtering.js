// As we saw yesterday, JavaScript does not have list comprehensions like Python, but it does have an Array method called filter, which works like the if in the Python list comprehension. Here we'll duplicate the filter examples from the previous question, but with the JavaScript filter method.

// Here's an example that selects all of the numbers from a list that are greater than 4.

// numbers = [1,2,3,4,5,6,7,8]

// greaterThan4 = numbers.filter(function(number) { return number > 4; });

// console.log(greaterThan4)  # --> [5,6,7,8]
// This example will select all of the strings that represent a number:

// strings = ["cat", "1", "a", "5.3"]

// numberStrings = strings.filter(function(string) {
//     return ! isNaN(parseFloat(string));
// });

// console.log(numberStrings)  # --> ["1", "5.3"]
// Implement the function shorterThanX(strings, maxLength) in JavaScript using the filter function.

const shorterThanX = function(strings, maxLength) {
    return strings.filter(item => item.length < maxLength)
};





// Please complete the following function that takes a list of values and a threshold value and returns a new list that contains entries from the original list that are less than or equal to the threshold value.

// Values	Threshold	Output
// []	7	[]
// [1, 10, 20]	7	[1]
// [1, 10, 20]	10	[1, 10]
// [20, 30, 40]	10	[]
// [20, 30, 40]	10	[]
// ['a', 'd', 'z']	"c"	['a']

function filter_less_than_or_equal_to(values, threshold) {
    return values.filter(value => value <= threshold)
}